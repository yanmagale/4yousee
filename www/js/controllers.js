angular.module('timelinemedia.controllers', [])

.controller('LoginCtrl', function($scope, $state) {

  $scope.login = function(){
    $state.go('tab.timeline');
  };
})

.controller('TimelineCtrl', function($scope, $http,$ionicLoading) {
  $scope.index = function(){
    $ionicLoading.show();
    $http({
      method: 'GET',
      url: 'https://private-7cf60-4youseesocialtest.apiary-mock.com/timeline'
    })
    .then(function(response) {
      $scope.medias = response.data;
      $ionicLoading.hide();
    })
    .catch(function(){
      console.log('Error in request');
      $ionicLoading.hide();
    });
  }

  $scope.getIconByType = function(type){
    var icon;
    switch (type) {
      case 'video':
        icon = 'ion-videocamera';
        break;
      case 'image':
        icon = 'ion-image';
        break;
      case 'rss':
        icon = 'ion-social-rss';
        break;
      default:
        icon = 'ion-videocamera';
    }
    return icon;
  }
});
