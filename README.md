# Timeline Media

> Veja suas midias através de uma timeline


## Sobre o projeto

Projeto criado para prova de conceito, no processo seletivo da 4YouSee . Consiste em um app para exibir, em um feed de timeline, as midias de um determinado usuário.

## Como executar
- Clone o projeto
- `npm install && bower install && ionic state reset`
- Rodar via browser: `ionic serve`
- Para gerar apk: `ionic run android`


## Roadmap

  Como o teste não exigia uma certa dependência sobre as telas, acabei utilizando o componente `ion-tabs` do ionic para poder transmitir a experiência de uma maneira mais rápida.


  Algumas funcionalidades que queria ter feito no projeto, mas que acabei não fazendo devido ao tempo reservado para desenvolver o teste:

### App
- Tela de detalhes do post
- Splashscreen/Icon 4YouSee
- Melhor escolha de fontWeight
- Prover internacionalização

### Code
- Melhora no algoritmo de request para pegar quantidades parciais
- Implementar validações com o JSHint/Eslint
- Separar os assets JS, arquivos de route
- Criar diretiva para renderizar as medias
- Incrementar task de build, com o gulp, e nos hooks do ionic
- Oferecer experiência offline com o `pouchdb`
- Utilizar o `Restangular` para REST API
- Passar as chamadas a API como `service`
- Testes Unitários
